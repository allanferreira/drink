const express = require('express')

const app  = express()
const port = process.env.PORT || 3000

app.use('/dist', express.static(`${__dirname}/dist`))

app.get('/', (req, res) => res.sendFile(`${__dirname}/index.html`))
app.listen(port, () => console.log(`running in ${port}`))